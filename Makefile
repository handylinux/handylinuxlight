# handylinuxlight makefile arnault perret <arpinux@member.fsf.org>

all: 
	@echo "Usage: "
	@echo "make 486       : build handylinuxlight-486"
	@echo "make 686       : build handylinuxlight-686-pae"
	@echo "make clean     : clean up build directories"
	@echo "make cleanfull : clean up build & cache directories"

486: clean
	@echo "-----------------------"
	@echo "building HandyLinux-486"
	@echo "-----------------------"
	cp ./auto/config-486 ./auto/config
	sudo lb build

686: clean
	@echo "--------------------------------"
	@echo "building HandyLinuxlight-686-pae"
	@echo "--------------------------------"
	cp ./auto/config-686 ./auto/config
	sudo lb build

clean:
	@echo "--------------------------"
	@echo "cleaning build directories"
	@echo "--------------------------"
	sudo lb clean
	sudo rm -f ./auto/config

cleanfull: clean
	@echo "----------------------------------"
	@echo "cleaning build & cache directories"
	@echo "----------------------------------"
	sudo rm -R -f cache
	sudo rm -f ./*.log
	sudo rm -f ./auto/config

