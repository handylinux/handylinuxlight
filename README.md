HANDYLINUXLIGHT
===============

La version allégée d'**HandyLinux** (-700 Mo)

- page dédiée : http://handylinux.org/documentation/doku.php/handylinuxlight
- dedicated page : http://handylinux.org/documentation-en/doku.php/handylinuxlight

- site principal :      <http://handylinux.org>
- forum d'entraide :    <http://handylinux.org/forum>
- documentation fr/en : <http://handylinux.org/documentation> & <http://handylinux.org/documentation-en>
- blog du projet :      <http://handylinux.org/blog>

- dernière version stable : handylinuxlight-1.8-manon


**HandyLinux** c'est l'accessibilité pour tous et la liberté pour chacun d'évoluer à son gré.

Basée sur **Debian GNU/Linux** avec XFCE, un environnement de bureau rapide, léger et stable, HandyLinux est **sûre, pratique et gratuite**.

Conçue pour faciliter l'accès à l'informatique à ceux qui débutent, les enfants, les seniors et ceux qui sont en quête de simplicité. Pour cela elle comprend :

- un pack simplifié d'applications pour les tâches courantes : (navigation sur le net, mail, suite office, logiciels photo/audio/vidéo, skype) facilement accessibles depuis le HandyMenu
- la logithèque Debian riche de milliers d'applications pour les aventuriers.
- une aide complète en ligne et une initiation intégrée à la distribution afin de répondre à vos questions et accompagner votre progression
- Si vous êtes en situation de handicap la rubrique "outils" est là pour faciliter l'accès aux différentes fonctions de l'ordinateur : loupe d'écran minimale, clavier virtuel et synthèse vocale.


matériel requis
---------------
**HandyLinux** s'installe sur tout ordinateur moderne (PIV ou supérieur) pourvu de 512M de mémoire, nécessite 3,7 GB d'espace disque minimum.

applications incluses
---------------------
- environnement de bureau : XFCE
- gestionnaire de fichiers : Thunar
- navigateur internet : Iceweasel-release (firefox)
- client de messagerie : Icedove (thunderbird)
- communication : Skype
- lecteur vidéo : VLC
- lecteur audio : QuodLibet
- simple radio : RadioTray
- visionneur d'images : Shotwell
- suite bureautique : LibreOffice
- éditeur de texte : Leafpad
- gestionnaire d'archives : File-roller
- aide à distance : TeamViewer
- suite d'outils pour l'accessibilité : loupe d'écran, clavier virtuel, menu simplifié
- gestionnaire d'impression et de numérisation
- aide française complète en ligne et initiation intégrée

changelog 1.7 -> 1.8
--------------------
- suppression du dépôt videolan, prise en charge de libdvdcss2 (merci coyotus)
- mise à jour d'Iceweasel en version 35
- désactivation de quelques plugins iceweasel pour améliorer le lancement (merci Xanatos)
- changement du moteur de recherche par défaut (StartPage) et ajout de moteurs (wikipedia-fr, searx, dailymotion...)
- mis à jour du handymenu : ajout des liens wikipédia et wallpapers
- ajout de gpart pour la récupération simple de données (merci dyp)
- suppression des musiques non-libres (merci On)
- ajout du lien vers Jamendo
- suppression des méta-paquets par défaut, ne reste qu'handylinux-desktop
- suppression de Skype des sources, mais toujours présent dans l'ISO.
- suppression de Teamviewer des sources
- ajout d'un installer pour Teamviewer en version 10 (merci coyotus)
- ajout de thèmes pour curseur (merci bruno, dyp et Ruz)
- ajout de thèmes gtk/xfwm Tron (merci Millie) et Pluto-gtk
- mise à jour redshift-config (merci Starsheep)
- ajout de pulseaudio pour skype et/ou teamviewer (merci coyotus)
- mise en place d'une nouvelle page d'accueil (merci Tiberias)

changelog 1.6.1 -> 1.7
----------------------
- passage à iceweasel-release comme navigateur par défaut
- ajout du firmware-ralink
- ajout de gpart pour la fonction de récupération de gparted
- ajout de yelp pour l’aide des logiciels
- nettoyage de la doc en fonction de la langue
- suppression du lanceur facebook au profit de framasoft
- ajout des lanceurs sociaux
- ajout de lien Diaspora
- ajout du thème tron par Millie
- mises à jour Debian

changelog 1.6 -> 1.6.1
----------------------
- mise à jour de skype en version 4.3
- suppression de l'appartenance à sudo pour les users supplémentaires
- passage de aspell/myspell à hunspell
- ajout de exfat-utils et exfat-fuse
- ajout du plugin de sélecteur de clavier xfce4-xkb-plugin
- refonte du processus de build pour régler le bug de l'autoremove et des méta-paquets
- mises à jour Debian

changelog 1.5 -> 1.6
--------------------
- ajout de l'option "addons" pour une post-installation hors-ligne
- amélioration du menu d'accessibilité (merci Irina)
- ajout de BigBuckBunny en exemple vidéo
- fusion des deux versions fr/en
- ajout du lancement live en mode sans échec
- ajout d'un sélecteur de clavier en session live
- intégration des méta-paquets handylinux-xxx
- ajout du fichier apt/preferences pour libreoffice en backports
- test version amd64 puis abandon car aucun besoin pour notre distribution
- retour du sélecteur de clavier à l'installation pour la version francophone
- passage à icedtea-7-plugin (merci coyotus)
- ajout de pepperflashplugin-nonfree et ttf-xfree86-nonfree pour chromium (merci dyp)
- passage au handymenu-2 avec outil de configuration intégré (merci manon)
- ajout des dépôts HandyLinux 'coming' commentés par défaut
- passage du handytri en paquet Debian
- ajout de hpijs-ppds pour les fichiers ppd (merci dYp)
- ajout de libmtp9 et libmtp-runtime (exit les warning modprobe, merci dYp)
- ajout de jmtpfs, mtp-tools et python-pymtp pour le transfert MTP (merci dyp)
- ajout magicfilter, djtools, librecode0, recode et lpr pour l'envoi à l'impression directe (merci dYp)
- refonte du processus de build
- mise à jour de teamviewer
- mise à jour de skype
- mise à jour Debian
- ajout de mscorefonts-installer
- ajout des outils iotop/iftop/dfc/colortail/most (merci lexdu)

changelog 1.4.2 -> 1.5
----------------------
- mise à jour de sécurité Debian
- mise à jour du noyau Linux
- mise à jour Chromium
- mise à jour du kernel
- ajout de l'invite de mise à jour au premier redémarrage
- mise à jour du handy-menu
- intégration du whiskermenu comme menu alternatif
- mise à jour de redshift-config
- ajout de fonts-opendyslexic
- ajout de cheese pour la webcam

changelog 1.4.1 -> 1.4.2
------------------------
- ajout de dvd+rw-tools, asunder et oggconvert
- entrée en piste des dépôts handylinux
- passage du filtre d'écran à redshift-config (config auto) par manon
- passage des lanceurs en exo-open dans le handy-menu et les lanceurs
- retour sur gitorious pour la gestion du code
- passage à system-config-printer pour la gestion de l'impression
- passage de handy-menu, slingscold et mpartage en installation externe
- ajout de gnome-font-viewer pour la gestion des polices de caractères
- ajout de l'outil de configuration pour le handymenu par manon
- suppression des lignes inutiles dans le touchpad-tap.sh
- ajout de hddtemps et lm-sensors
- rétablissement de la dernière étape de l'installeur Debian (retirer la clé usb)
- ajout du fichier network.mount pour éviter le montage automatique du réseau dans thunar

changelog 1.4 -> 1.4.1
----------------------
- bug du magnifier avec nvidia => rétablissement de la loupe d'écran classique
- rétablissement du panel de la 1.3

changelog 1.3 -> 1.4
--------------------
- conservation de GParted après installation
- passage de xsltproc dans les paquets
- ajout de blueman
- ajout de libasound2-plugins pour skype
- ajout de [xl-wallpaper](http://xl-wallpaper.net) de **Christophe Cagé**
- intégration des modifications redshit par **firepowi**
- ajout de gnome-system-tools pour la gestion des utilisateurs
- remplacement du lanceur "twitter" par "minitube"
- remplacement de xsane par simple-scan
- ajout de python-lxml pour la gestion du handymenu
- handy-menu : passage aux commandes "exo-open" + ajout de la persistance par **manon**
- coupure du réseau pendant l'install pour éviter les plantage en cas d'incident réseau
- ajout de mpartage par **manon**
- confguration du NumLockx intégré dans slimconf par **manon**
- inclure tap-to-clic.sh désactivé
- rétablissemement du réglage du hostname dans l'installeur
- passage à libreoffice-4(backports)
- ajout du script de triage automatique
- ajout du plugin xfce météo

changelog 1.2 -> 1.3
--------------------
- une seule instance pour le HandyMenu
- virer speed-dial à l'ouverture de chromium
- intégrer redshift avec un sélecteur de villes
- intégrer la documentation en format pdf dans la distribution pour une consultation hors-ligne
- remplacer squeeze par file-roller
- simplifier le handymenu
- remplacer gpicview par shotwell
- ajouter gparted dans le livecd
- ajouter minitube
- ajout de la gestion du bluetooth
- passage à teamviewer9
- changer les entrées handymenu avec les entrées génériques
- intégration du greffon de réglage du volume dans le panel
- une seule instance de la loupe d'écran
- ajouter un lanceur "negative" aux outils : xcalib

