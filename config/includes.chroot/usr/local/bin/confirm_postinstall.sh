#!/bin/bash

# détection de la langue
TRAD=`echo "${LANG:0:2}"`
# processus fr
if [ "${TRAD}" == "fr" ]; then
# session live : avertissement
	if [ -d /home/humain ]; then
        /usr/local/bin/keyboard_selector
		zenity --info --text "Bienvenue sur HandyLinuxLight\n\nCette version est incomplète mais vous permet de vérifier\nla compatibilité de votre matériel.\n\nPour installer HandyLinux :\nRedémarrer votre ordinateur puis choisir\n\"Installer HandyLinux\"\n\nPlus d'informations sur http://handylinux.org"
	else
# session installée : fix bookmarks & nettoyage
		echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Images"  >> /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Musique"  >> /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/T%C3%A9l%C3%A9chargements" >> /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Vid%C3%A9os" >> /home/$USER/.gtk-bookmarks
		rm /home/$USER/.magnifier.ini.en /home/$USER/.config/user-dirs.dirs.en
# session installée : demande de post-installation
		ACTION=`zenity --question --title "HandyLinux -- Post-installation" --text " Bienvenue sur HandyLinuxLight\n\n Cette version est incomplète\n vous devez lancer la post-installation\n afin de pouvoir profiter d'HandyLinux\n\n Voulez-vous continuer ?"; ans=$? ; echo "$ans"`
		if [ "${ACTION}" == "0" ]; then
# lancement de la post-installation
			xfce4-terminal -e /usr/local/bin/handylinuxlight_postinstall.sh
		else
# annulation de la post-installation
			zenity --info --text "Post-installation annulée\n Le script se lancera au prochain login\n\n Vous pouvez le lancer depuis un terminal:\n \"sudo /usr/local/bin/handylinuxlight_postinstall.sh\""
		fi
	fi
else
# processus en
	#fix folders names
	if [ -d /home/$USER/Images ]; then
		mv /home/$USER/Images /home/$USER/Pictures
		mv /home/$USER/Musique /home/$USER/Music
		mv /home/$USER/Téléchargements /home/$USER/Downloads
		mv /home/$USER/Vidéos /home/$USER/Videos
		#fix bookmarks
		echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Pictures"  >> /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Music"  >> /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Downloads" >> /home/$USER/.gtk-bookmarks
		echo "file:///home/$USER/Videos" >> /home/$USER/.gtk-bookmarks
		#fix config-en
		rm /home/$USER/.config/user-dirs.dirs
		mv /home/$USER/.config/user-dirs.dirs.en /home/$USER/.config/user-dirs.dirs
		rm /home/$USER/.config/user-dirs.locale
		rm /home/$USER/.magnifier.ini
		mv /home/$USER/.magnifier.ini.en /home/$USER/.magnifier.ini
	fi
# live session : warning
	if [ -d /home/human ]; then
        /usr/local/bin/keyboard_selector
		#zenity message
		zenity --info --text "Welcome to HandyLinuxLight\n\nThis version miss some packages but you can check\nyour hardware or network compatibility.\n\nTo install HandyLinux :\nReboot your computer then choose\n\"Install HandyLinux\"\n\nMore informations: http://handylinux.org"
	else
# installed session : launch post-installation
		ACTION=`zenity --question --title "HandyLinux -- Post-installation" --text " Welcome to HandyLinuxLight\n\n This version miss packages\n you have to launch the post-installation\n to completly enjoy HandyLinux\n\n Do you want to continue ? [Y|n]"; ans=$? ; echo "$ans"`
		if [ "${ACTION}" == "0" ]; then
# post-installation launch
			xfce4-terminal -e /usr/local/bin/handylinuxlight_postinstall.sh
		else
# post-installation avorted
			zenity --info --text "Post-installation avorted by user\n The script will be launched on next login\n\n You can launch it from a terminal:\n \"sudo /usr/local/bin/handylinuxlight_postinstall.sh\""
		fi
	fi
fi
exit 0
