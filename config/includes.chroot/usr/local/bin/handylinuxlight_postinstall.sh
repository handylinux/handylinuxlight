#!/bin/bash
# handylinuxlight post-install script
# -----------------------------------

# set colors
# ----------
RED='\e[0;31m'
NC='\e[0m'

# set lang
# --------
TRAD=`echo "${LANG:0:2}"`

# postinstall fr
if [ "${TRAD}" == "fr" ]; then
    echo -e "  ----------------------------------------------------------  "
    echo -e "  Bienvenue dans le script de post-installation d'HandyLinux  "
    echo -e "  ----------------------------------------------------------\n"
    echo -e " Ce script va installer les paquets manquants indispensables\n à HandyLinux, les fonds d'écran et les paquets indépendants\n\n "
    echo -e " Vous pourrez suivre le processus sur ce terminal.\n Votre mot de passe vous sera demandé.\n\n"
    echo -e "$RED Ce script demande une connection internet active. $NC\n"
    echo -e "$RED Si vous n'avez pas de connexion internet$NC\n vous pouvez utiliser le \"HandyLinuxLight-addonCD\"."
    echo -e " Dans ce cas,$RED Refusez$NC la post-installation\n et insérez l'addonCD dans votre lecteur.\n "
    echo " Voulez-vous continuer [O|n] ?"
    echo -n " >  "
    read ans
    if [ "$ans" == "N" ] || [ "$ans" == "n" ]; then
        echo -e "\n$RED script annulé "
        sleep 4s && exit 0
    else
        echo -e "\n ----------------------\n Mise à jour des dépôts "
        echo -e " ----------------------\n"
        sleep 2s && sudo apt-get update
        echo -e "\n -----------------------\n Mise à jour des paquets "
        echo -e " -----------------------\n"
        sleep 2s && sudo apt-get dist-upgrade -y
        echo -e "\n ---------------------------------\n Installation des paquets manquants "
        echo -e " --------------------------------- \n"
        sleep 2s && sudo apt-get install -y gnupg user-setup desktop-base locales console-setup console-common numlockx xorg acpi most dfc colortail iotop iftop htop x11-utils x11-apps handy-menu handytri slingscold-launcher redshift-config mpartage xl-wallpaper xfce4 slim xfce4-terminal vrms file-roller dtrx zip eject xscreensaver xscreensaver-data-extra python-dbus python-gtk2 python-notify python-pymtp python-xdg xdg-utils libnotify-bin usbutils xsltproc libmtp9 libmtp-runtime jmtpfs mtp-tools dosfstools ntfs-3g mtools ntfsprogs e2fsprogs gnome-disk-utility gparted gpart sudo gksu synaptic software-center update-notifier os-prober gnome-system-tools lsb-release gvfs gvfs-bin gvfs-fuse gvfs-backends hal gnome-keyring libpam-gnome-keyring policykit-1-gnome pam-dbus-notify libpam-ck-connector accountsservice xdotool bleachbit hardinfo libjpeg62 oneko fuse-utils fusesmb netatalk xcalib gnome-search-tool hddtemp lm-sensors libunique-1.0-0 libgnome-menu2 rpl xfce4-battery-plugin xfce4-weather-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin xfce4-volumed xfce4-mount-plugin xfce4-notifyd xfce4-mailwatch-plugin xfce4-places-plugin xfce4-power-manager-plugins xfce4-sensors-plugin xfce4-settings xfce4-taskmanager thunar-archive-plugin thunar-volman tumbler flashplugin-nonfree firmware-b43-installer firmware-b43legacy-installer b43-fwcutter firmware-linux firmware-linux-nonfree alsa-firmware-loaders intel-microcode iucode-tool ttf-mscorefonts-installer ttf-xfree86-nonfree unrar shotwell simple-scan murrine-themes gtk2-engines gtk2-engines-pixbuf gtk3-engines-unico gtk2-engines-aurora gtk2-engines-oxygen gnome-themes-standard gnome-icon-theme-symbolic dmz-cursor-theme gnome-font-viewer xfce4-artwork xfce4-screenshooter xfwm4-themes fonts-droid fonts-liberation alsa-base alsa-utils alsa-tools vlc quodlibet quodlibet-plugins radiotray asunder oggconvert flac wavpack lame dvd+rw-tools libdvdcss2 cheese xfburn phonon-backend-vlc phonon-backend-gstreamer libphonon4 libasound2-plugins python-feedparser python-gpod gstreamer0.10-ffmpeg gstreamer0.10-plugins-bad gstreamer0.10-plugins-base gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-x gstreamer0.10-fluendo-mp3 gstreamer0.10-alsa gstreamer0.10-pulseaudio pulseaudio pavucontrol icedove icedtea-7-plugin iproute iptables net-tools wpasupplicant wireless-tools pppoeconf ntp network-manager-gnome network-manager-openvpn-gnome network-manager-vpnc-gnome bluez gnome-bluetooth blueman linux-wlan-ng firmware-linux-free libspeechd2 leafpad evince xpad gcalctool cups hplip hplip-gui hpijs-ppds printer-driver-c2050 printer-driver-c2esp printer-driver-cjet printer-driver-escpr openprinting-ppds gutenprint-locales printer-driver-gutenprint printer-driver-hpcups printer-driver-postscript-hp printer-driver-m2300w printer-driver-min12xxw printer-driver-pnm2ppa printer-driver-ptouch printer-driver-sag-gdi printer-driver-splix printer-driver-foo2zjs printer-driver-hpijs printer-driver-pxljr system-config-printer magicfilter djtools librecode0 recode lpr xfprint4 lsb xscreensaver-gl xscreensaver-gl-extra mahjongg aisleriot gbrainy gnome-sudoku xfonts-100dpi xfonts-75dpi xfonts-base xfonts-terminus vmg exfat-utils exfat-fuse imagemagick librsvg2-bin zenity python-lxml xfce4-xkb-plugin handylinux-desktop libreoffice-l10n-fr icedove-l10n-fr hunspell-fr manpages-fr manpages-fr-extra slimconf debian-reference-fr live-tools libxss1
        echo -e "\n ------------------------------------------------\n Installation de LibreOffice depuis les backports "
        echo -e " ------------------------------------------------\n"
        sleep 2s && sudo apt-get install -y -t wheezy-backports libreoffice libreoffice-gtk
        echo -e "\n ------------------------------------------------\n Installation de Iceweasel depuis les dépôts Mozilla "
        echo -e " ------------------------------------------------\n"
        sleep 2s && echo "deb http://mozilla.debian.net/ wheezy-backports iceweasel-release" >> mozilla.list
        sudo mv mozilla.list /etc/apt/sources.list.d/mozilla.list
        sudo apt-get update && sudo apt-get install --allow-unauthenticated pkg-mozilla-archive-keyring
        sudo apt-get update && sudo apt-get install -y --force-yes -t wheezy-backports iceweasel iceweasel-l10n-fr
        sudo apt-get install handylinux-desktop
        echo -e "\n -------------------------------------\n Installation des paquets indépendants "
        echo -e " -------------------------------------\n"
        echo -e " Installation de Skype\n"
        sleep 2s && wget http://handylinux.org/stock_files/handylinuxlight/skype-debian_4.3.0.37-1_i386.deb
        sudo dpkg -i skype-debian_4.3.0.37-1_i386.deb
        rm skype-debian_4.3.0.37-1_i386.deb
        echo -e "\n Installation de Minitube\n"
        sleep 2s && wget http://handylinux.org/stock_files/handylinuxlight/minitube_2.0-3_i386.deb
        sudo dpkg -i minitube_2.0-3_i386.deb
        rm minitube_2.0-3_i386.deb
        echo -e "\n -------------------------------------------\n Récupération de la documentation et des walls HandyLinux "
        echo -e " -------------------------------------------\n"
        wget http://handylinux.org/stock_files/handylinuxlight/HandyLinux_doc.tar.gz
        wget http://handylinux.org/stock_files/handywalls.zip
        sudo mv HandyLinux_doc.tar.gz /usr/share/handylinux/HandyLinux_doc.tar.gz
        sudo mv handywalls.zip /usr/share/xfce4/backdrops/handywalls.zip
        cd /usr/share/xfce4/backdrops/ && sudo unzip handywalls.zip
        sudo rm handywalls.zip
        cd /usr/share/handylinux/ && sudo tar xvzf HandyLinux_doc.tar.gz
        sudo rm ./HandyLinux_doc.tar.gz && cd ~
        echo -e "\n ---------------------\n Remplacement du lanceur "
        echo -e " ---------------------"
        rm /home/$USER/.config/autostart/postinstall.desktop
        sudo mv /usr/share/handylinux/welcome.desktop /home/$USER/.config/autostart/welcome.desktop
        sudo chown $USER:$USER /home/$USER/.config/autostart/welcome.desktop
        echo "HandyLinuxLight - online-post-install - `date`" > /home/$USER/.handypost.log
    fi
    echo -e "\n\n La post-installation est achevée.\n\n"
    echo -e " Vous pouvez désormais profiter pleinement\n  de votre distribution HandyLinux\n\n"
    echo -e " si une erreur est survenue, notez les messages\n du terminal et rendez-vous sur le forum\n http://handylinux.org/forum .\n\n"
    echo -e " Appuyez sur [Enter] pour quitter."
    read anykey
else
    echo -e "  ---------------------------------------------------------  "
    echo -e "     Welcome to HandyLinuxLight Post-Installation Script  "
    echo -e "  ---------------------------------------------------------\n"
    echo -e " This script will install missing package in HandyLinuxLight\n and some extra stuff like wallpapers\n\n"
    echo -e " you could follow all process in this terminal.\n Your password will be asked.\n\n"
    echo -e "$RED This script needs an active internet connection. $NC\n"
    echo -e "$RED If you don't have internet connection,$NC\n you can use the \"HandyLinuxLight-addonCD\"."
    echo -e " In that case,$RED Refuse$NC post-installation\n and insert the addonCD in your CD player.\n "
    echo -e " Do you want to continue [Y|n] ?"
    echo -n " >  "
    read ans
    if [ "$ans" == "N" ] || [ "$ans" == "n" ]; then
        echo -e "$RED script avorted "
        sleep 4s && exit 0
    else
        echo -e "\n --------------------\n Updating Repositories "
        echo -e " --------------------\n"
        sleep 2s && sudo apt-get update
        echo -e "\n ----------------\n Updating packages "
        echo -e " ----------------\n"
        sleep 2s && sudo apt-get dist-upgrade -y
        echo -e "\n --------------------------\n Installing missing packages "
        echo -e " --------------------------\n"
        sleep 2s && sudo apt-get install -y gnupg user-setup desktop-base locales console-setup console-common numlockx xorg acpi most dfc colortail iotop iftop htop x11-utils x11-apps handy-menu handytri slingscold-launcher redshift-config mpartage xl-wallpaper xfce4 slim xfce4-terminal vrms file-roller dtrx zip eject xscreensaver xscreensaver-data-extra python-dbus python-gtk2 python-notify python-pymtp python-xdg xdg-utils libnotify-bin usbutils xsltproc libmtp9 libmtp-runtime jmtpfs mtp-tools dosfstools ntfs-3g mtools ntfsprogs e2fsprogs gnome-disk-utility gparted gpart sudo gksu synaptic software-center update-notifier os-prober gnome-system-tools lsb-release gvfs gvfs-bin gvfs-fuse gvfs-backends hal gnome-keyring libpam-gnome-keyring policykit-1-gnome pam-dbus-notify libpam-ck-connector accountsservice xdotool bleachbit hardinfo libjpeg62 oneko fuse-utils fusesmb netatalk xcalib gnome-search-tool hddtemp lm-sensors libunique-1.0-0 libgnome-menu2 rpl xfce4-battery-plugin xfce4-weather-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin xfce4-volumed xfce4-mount-plugin xfce4-notifyd xfce4-mailwatch-plugin xfce4-places-plugin xfce4-power-manager-plugins xfce4-sensors-plugin xfce4-settings xfce4-taskmanager thunar-archive-plugin thunar-volman tumbler flashplugin-nonfree firmware-b43-installer firmware-b43legacy-installer b43-fwcutter firmware-linux firmware-linux-nonfree firmware-ralink alsa-firmware-loaders intel-microcode iucode-tool ttf-mscorefonts-installer ttf-xfree86-nonfree unrar shotwell simple-scan murrine-themes gtk2-engines gtk2-engines-pixbuf gtk3-engines-unico gtk2-engines-aurora gtk2-engines-oxygen gnome-themes-standard gnome-icon-theme-symbolic dmz-cursor-theme gnome-font-viewer xfce4-artwork xfce4-screenshooter xfwm4-themes fonts-droid fonts-liberation alsa-base alsa-utils alsa-tools vlc quodlibet quodlibet-plugins radiotray asunder oggconvert flac wavpack lame dvd+rw-tools libdvdcss2 cheese xfburn phonon-backend-vlc phonon-backend-gstreamer libphonon4 libasound2-plugins python-feedparser python-gpod gstreamer0.10-ffmpeg gstreamer0.10-plugins-bad gstreamer0.10-plugins-base gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-x gstreamer0.10-fluendo-mp3 gstreamer0.10-alsa gstreamer0.10-pulseaudio pulseaudio pavucontrol icedove icedtea-7-plugin iproute iptables net-tools wpasupplicant wireless-tools pppoeconf ntp network-manager-gnome network-manager-openvpn-gnome network-manager-vpnc-gnome bluez gnome-bluetooth blueman linux-wlan-ng firmware-linux-free libspeechd2 leafpad evince xpad gcalctool cups hplip hplip-gui hpijs-ppds printer-driver-c2050 printer-driver-c2esp printer-driver-cjet printer-driver-escpr openprinting-ppds gutenprint-locales printer-driver-gutenprint printer-driver-hpcups printer-driver-postscript-hp printer-driver-m2300w printer-driver-min12xxw printer-driver-pnm2ppa printer-driver-ptouch printer-driver-sag-gdi printer-driver-splix printer-driver-foo2zjs printer-driver-hpijs printer-driver-pxljr system-config-printer magicfilter djtools librecode0 recode lpr xfprint4 lsb xscreensaver-gl xscreensaver-gl-extra mahjongg aisleriot gbrainy gnome-sudoku xfonts-100dpi xfonts-75dpi xfonts-base xfonts-terminus vmg exfat-utils exfat-fuse imagemagick librsvg2-bin zenity python-lxml xfce4-xkb-plugin yelp hunspell-en-us debian-reference-en live-tools libxss1
        echo -e "\n ------------------------------------------------\n Installing LibreOffice from backports "
        echo -e " ------------------------------------------------\n"
        sleep 2s && sudo apt-get install -y -t wheezy-backports libreoffice libreoffice-gtk
        echo -e "\n ------------------------------------------------\n Installing Iceweasel from Mozilla repository"
        echo -e " ------------------------------------------------\n"
        sleep 2s && echo "deb http://mozilla.debian.net/ wheezy-backports iceweasel-release" >> mozilla.list
        sudo mv mozilla.list /etc/apt/sources.list.d/mozilla.list
        sudo apt-get update && sudo apt-get install --allow-unauthenticated pkg-mozilla-archive-keyring
        sudo apt-get update && sudo apt-get install -y --force-yes -t wheezy-backports iceweasel
        sudo apt-get install handylinux-desktop
        echo -e "\n -------------------------------------\n Installing external packages "
        echo -e " -------------------------------------\n"
        echo -e " Installing Skype \n"
        sleep 2s && wget http://handylinux.org/stock_files/handylinuxlight/skype-debian_4.3.0.37-1_i386.deb
        sudo dpkg -i skype-debian_4.3.0.37-1_i386.deb
        rm skype-debian_4.3.0.37-1_i386.deb
        echo -e "\n Installing Minitube\n"
        sleep 2s && wget http://handylinux.org/stock_files/handylinuxlight/minitube_2.0-3_i386.deb
        sudo dpkg -i minitube_2.0-3_i386.deb
        rm minitube_2.0-3_i386.deb
        echo -e "\n ----------------------------\n Get HandyLinux Documentation and walls"
        echo -e " ----------------------------\n"
        wget http://handylinux.org/stock_files/handylinuxlight/HandyLinux_doc.tar.gz
        wget http://handylinux.org/stock_files/handywalls.zip
        sudo mv HandyLinux_doc.tar.gz /usr/share/handylinux/HandyLinux_doc.tar.gz
        sudo mv handywalls.zip /usr/share/xfce4/backdrops/handywalls.zip
        cd /usr/share/handylinux/ && sudo tar xvzf HandyLinux_doc.tar.gz
        sudo rm HandyLinux_doc.tar.gz
        cd /usr/share/xfce4/backdrops/ && sudo unzip handywalls.zip
        sudo rm handywalls.zip
        echo -e "\n --------------------\n Set welcome launcher "
        echo -e " --------------------"
        rm /home/$USER/.config/autostart/postinstall.desktop
        sudo mv /usr/share/handylinux/welcome.desktop /home/$USER/.config/autostart/welcome.desktop
        sudo chown $USER:$USER /home/$USER/.config/autostart/welcome.desktop
        echo "HandyLinuxLight - online-post-install - `date`" > /home/$USER/.handypost.log
    fi
    echo -e "\n\n  Post-installation is finish.\n\n"
    echo -e " You can now enjoy HandyLinux\n\n"
    echo -e " if a bug happend, please take note of the bug message\n then come to the forum, we'll find a way out :)"
    echo -e " http://handylinux.org/forum .\n\n"
    echo -e " Press [Enter] to quit."
    read anykey
fi
exit 0
