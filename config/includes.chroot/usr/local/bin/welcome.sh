#!/bin/bash
# welcome script and fix bookmarks
##################################
# détection de la langue
TRAD=`echo "${LANG:0:2}"`

if [ "${TRAD}" == "fr" ]; then
    #langue française
    cd /usr/local/bin/welcome
    #message d'accueil
    ./welcome.py
    #nettoyage
    rm /home/$USER/.config/autostart/welcome.desktop
else
    #english language
    cd /usr/local/bin/welcome-en
    #welcome message
    ./welcome.py
    #cleaning
    rm /home/$USER/.config/autostart/welcome.desktop
fi
exit 0
