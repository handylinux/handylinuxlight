#!/bin/bash

# get wav file path and extension file paths, passed as command line args
extpath=$1
wavpath=$2

# remove marker indicating to extension that wav file creation has completed
rm -v $extpath/wavdone.txt #>/dev/null
rm -v $extpath/nowav.txt #>/dev/null

# copy the temporary wave file to location specified by user
cp $extpath/speak.wav $wavpath
error=$?
echo "copy error: $error"

	if [ "$error" -ne "0" ]; then
	{
		#create marker file
		echo "copy wav problem" > $extpath/nowav.txt
	} fi

	if [ "$error" -eq "0" ]; then
	{
	echo "wav done" > $extpath/wavdone.txt
	} fi

