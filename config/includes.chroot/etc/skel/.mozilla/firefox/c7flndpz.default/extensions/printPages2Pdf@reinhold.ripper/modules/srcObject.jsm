Components.utils.import("resource://printPages2PdfMod/printPages2PdfGlobal.jsm"); 

var EXPORTED_SYMBOLS = [ "_srcObject","_tocObject", ];

/** 
 * printPages2Pdf namespace. 
 */  
 
if (typeof printPages2Pdf == "undefined") {  
  var printPages2Pdf = {};  
};

Components.utils.import("resource://printPages2PdfMod/domLoader.jsm",printPages2Pdf);
Components.utils.import("resource://printPages2PdfMod/persistWin.jsm",printPages2Pdf);
Components.utils.import("resource://printPages2PdfMod/win2Image.jsm",printPages2Pdf);
Components.utils.import("resource://printPages2PdfMod/textOnly.jsm",printPages2Pdf);
Components.utils.import("resource://printPages2PdfMod/srcObjectLight.jsm",printPages2Pdf);
Components.utils.import("resource://printPages2PdfMod/editPage.jsm",printPages2Pdf);


var _srcObject = function(location,bTextOnly){
	
	this.init(location,bTextOnly);
/*
	if(bTextOnly) this.isTextOnly=bTextOnly;

	if (location instanceof printPages2Pdf._srcObjectLight)  { //Light srcObject
		for(var p in location){
			this[p]=location[p];
		}
		
		if(! location.originUrl) 
			this.setOriginUrl(this._initLocation);
	}				
	else {
		this._initLocation=location;
		this.setOriginUrl(this._initLocation);
	}
	
*/
}



_srcObject.prototype = {

	init : function(location,bTextOnly){
	if(bTextOnly) this.isTextOnly=bTextOnly;

	if (location instanceof printPages2Pdf._srcObjectLight)  { //Light srcObject
		for(var p in location){
			this[p]=location[p];
		}
		
		if(! location.originUrl) 
			this.setOriginUrl(this._initLocation);
	}				
	else {
		this._initLocation=location;
		this.setOriginUrl(this._initLocation);
	}
	

},

	sourceType:"unknown",
	updatePrefs:null,
	_initLocation:null,
	originUrl:null,
	editUrl:null,
	imageUrl:null,
	originType:null,
	isImage:null,
	isTextOnly:false,
	_pageloadTimeout:null,
	_outlineTitleOnly:null,
	gPrefOptions:null,

	_localUrl:null,
	get localUrl(){
		if(!this._localUrl)
			this.preProcess();
		
		if(this.editUrl)
			return this.editUrl;
		else
			return this._localUrl;
	},
	
	set localUrl(url){
		this._localUrl=url;	
	},

	_customObjectPrefs:null,

	set customObjectPrefs(obj){
		this._customObjectPrefs=obj;
		
		
	},

	get customObjectPrefs(){
		return this._customObjectPrefs;
		
		
	},

	
	prepareCustomObjectsPref:function(){
		
		this._outlineTitleOnly=RRprintPages2Pdf.prefs.getCharPref("wkhtml.iopt.outline.titleOnly");
		this._pageloadTimeout=RRprintPages2Pdf.prefs.getCharPref("wkhtml.iopt.pageload.timeout");

		if(_srcObject.prototype.gPrefOptions){
			if(_srcObject.prototype.gPrefOptions["outline.titleOnly"])
				this._outlineTitleOnly=_srcObject.prototype.gPrefOptions["outline.titleOnly"];
			if(_srcObject.prototype.gPrefOptions["pageload.timeout"])
				this._pageloadTimeout=_srcObject.prototype.gPrefOptions["pageload.timeout"];
		}


		if (this._customObjectPrefs) {
			if(this._customObjectPrefs["outline.titleOnly"])
				this._outlineTitleOnly=this._customObjectPrefs["outline.titleOnly"];
		
			if(this._customObjectPrefs["pageload.timeout"])
				this._pageloadTimeout=this._customObjectPrefs["pageload.timeout"];
		}		
		
	},
	
	replaceWebpageTag:function(propName,propValue,content){
		
		
		var retVal=propValue;
		switch(propName){
			case "header.left":
			case "header.center":
			case "header.right":
			case "footer.left":
			case "footer.center":
			case "footer.right":
				retVal=propValue.replace(/\[webpage\]/,this.originUrl);
				break;
				
			case "header.htmlUrl":
			case "footer.htmlUrl":
				if(!content){
					var localFile = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
					localFile.initWithPath(propValue);
					content = RRprintPages2Pdf.ReadTextFile(localFile);					
				}
				if (RegExp(/\[webpage\]/).test(content)) {
					var newHdrFile=this.workDir.clone();
					newHdrFile.append("DocHdrFtr.html");	
					newHdrFile.createUnique(retVal.NORMAL_FILE_TYPE,666);
					RRprintPages2Pdf.WriteTextFile(newHdrFile,content.replace(/\[webpage\]/,this.originUrl));
					retVal=newHdrFile.path;
				}
				//make the hdr/ftr path to a file url = no problems with unicode
				var tmpFile = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
				tmpFile.initWithPath(retVal);
				var IO=Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService); 
				retVal = IO.newFileURI(tmpFile).spec;
								
				break;		
			
			default:
				break;
		}	
		
		return retVal;
	},
	
	getConverterPrefs:function(){
		var retVal={};

		for (var p in _srcObject.prototype.gPrefOptions) {
			retVal[p]=_srcObject.prototype.gPrefOptions[p];
		}


		if(this._customObjectPrefs){
			//copy object
			for (var p in this._customObjectPrefs) {
				retVal[p]=this._customObjectPrefs[p];
			}

			//remove all identical global defined properties
/*
			for(var p in retVal){
				if ((p in _srcObject.prototype.gPrefOptions) && retVal[p] == _srcObject.prototype.gPrefOptions[p]) {
					delete retVal[p];
				}
			}

*/
		}

		//remove internal props
		if(retVal["outline.titleOnly"])
			delete retVal["outline.titleOnly"];
		if(retVal["pageload.timeout"])
			delete retVal["pageload.timeout"];
	
		for(var p in retVal){
			retVal[p]=this.replaceWebpageTag(p,retVal[p])
		}

		
		if(! _srcObject.prototype.updatePrefs ) return retVal;

				
		//add new update Props
		for(var p in _srcObject.prototype.updatePrefs){
			if(!(p in retVal) && !RegExp(/^__data/).test(p)){
				if(("__data." + p) in _srcObject.prototype.updatePrefs)
					retVal[p]= this.replaceWebpageTag(p,_srcObject.prototype.updatePrefs[p],_srcObject.prototype.updatePrefs["__data." + p]);
				else
					retVal[p]= this.replaceWebpageTag(p,_srcObject.prototype.updatePrefs[p]);
			}
		}

		return retVal;

	},
	
	_favIconUrl:null,
	set favIconUrl(url){
		this._favIconUrl=url;	
	},
	
	__faviconService:null,
	get faviconService(){
		if(!_srcObject.prototype.__faviconService){
			_srcObject.prototype.__faviconService=Components.classes["@mozilla.org/browser/favicon-service;1"]
                     .getService(Components.interfaces.nsIFaviconService);
		}
		return _srcObject.prototype.__faviconService;
	},
	
	get asyncFaviconService(){

		return this.faviconService.QueryInterface(Components.interfaces.mozIAsyncFavicons);;
	},	
	
	__ioService:null,
	get ioService(){
		if(!_srcObject.prototype.__ioService){
			_srcObject.prototype.__ioService = Components.classes["@mozilla.org/network/io-service;1"]  
                  .getService(Components.interfaces.nsIIOService);  
		}
		return _srcObject.prototype.__ioService;
	},
	
	//changed to async updateFavIcon
	get favIconUrl(){
		return this._favIconUrl;
	},
	

	
	_Description:null,
	get Description(){
		if(this._Description) return this._Description;

		if(this.originUrl) return this.originUrl;
		if(this.localUrl)return this.localUrl;
		return null;
	},
	set Description(txt){
		this._Description=txt;
	},
	
	_Title:null,
	get Title(){
		if(this._Title) return this._Title;
		var aktUrl=null;
		if(this.originUrl) aktUrl=this.originUrl;
		else if(this.localUrl)aktUrl=this.localUrl;
		
		if(aktUrl){
			try {
				var uri=printPages2Pdf.saver.CommonUtils.IO.newURI(aktUrl, null, null);
				var url = uri.QueryInterface(Components.interfaces.nsIURL); 
				return (url.host?url.host + " :: ":"") + url.fileName;
			}
			catch(e) {/*not a valid Url*/};
		}
		
		return "no Title";
	},
	get suggestedFileName(){
		return printPages2Pdf.saver.CommonUtils.validateFileName(this.Title);	
	},
	
	set Title(txt){
		this._Title=txt;
	},
	
	
	_workDir:null,
	set workDir(dir){
		_srcObject.prototype._workDir=dir;
	},
	
	get workDir(){
		return _srcObject.prototype._workDir;
	},
	
	_parentWindow:null,
	set parentWindow(win){
		printPages2Pdf.domLoader._parentWin=win;
		_srcObject.prototype._parentWindow=win;
	},
	
	get parentWindow(){
		return _srcObject.prototype._parentWindow;
	},
	
	
	_domLoadCallback:null,
	set domLoadCallback(cb){
		_srcObject.prototype._domLoadCallback=cb;
	},
	
	get domLoadCallback(){
		return _srcObject.prototype._domLoadCallback;
	},

	_persistCallback:null,
	set persistCallback(cb){
		printPages2Pdf.saver.ContentSaver.actualCallback=cb;
		_srcObject.prototype._persistCallback=cb;
	},
	
	get persistCallback(){
		return _srcObject.prototype._persistCallback;
	},
	
	_canvas:null,
	set canvas(c){
		_srcObject.prototype._canvas=c;		
	},
	get canvas(){
		return _srcObject.prototype._canvas;		
	},
	
	setOriginUrl:function(obj){
		if (typeof(obj) == "string") {
			this.originUrl=obj;
		}
		else if (obj instanceof Components.interfaces.nsIDOMWindow)  { //DOM window
			this.originUrl=obj.location.href;
/*
			var me=this;
			obj.addEventListener("beforeunload",function(){
				me.handleEvent();
			},false);
*/
			obj.addEventListener("beforeunload",this,false);
		}
		
	},
	
	//ansync update of favicon, cb is async callback
	updateFavIcon:function(cb){
		if(this._favIconUrl){
			cb(this._favIconUrl, null, null, null);
			return;	
		}
		
		if (this.originUrl) {
			var uri = this.ioService.newURI(this.originUrl, null, null);
			if (this.faviconService.getFaviconImageForPage) {
				this._favIconUrl = this.faviconService.getFaviconImageForPage(uri).spec;
				cb(this._favIconUrl, null, null, null);
				
			}
			else {
				cb(this.faviconService.defaultFavicon.spec, null, null, null);
				this.asyncFaviconService.getFaviconURLForPage(uri, cb);
			}
		}
	},
	
	
	handleEvent:function(event){
		this._initLocation=this._saveWinLocal(this._initLocation);
		event.target.removeEventListener("beforeunload",this,false);
	},
	
	preProcess: function(){
		if (typeof(this._initLocation) == "string") {
			if (this._initLocation.search(/^file:/) != -1) {
				this.localUrl = this._initLocation;

				if (this.fileMustBeImage(this.localUrl)) 
					this.isImage = true;
			}
			else 
				this.localUrl = this._saveUrlLocal(this._initLocation);
			
		}
		else if (this._initLocation instanceof Components.interfaces.nsIDOMWindow)  { //DOM window
			this.localUrl=this._saveWinLocal(this._initLocation);
			this._initLocation.removeEventListener("beforeunload",this,false);
			
		}				
	},
	
	winMustBeImage : function(win){
		if(win.document){
			var framesets=win.document.getElementsByTagName("frameset");
			if( framesets && framesets.length > 0){
				return true;	
			}			
		}
		
		return false;
	},
	
	fileMustBeImage : function(url){
		
		var req=null;
		try {
			 req = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
			 req.open('POST', url, false);
			 req.send('');
		} catch(e) {}
		
		if(!req.responseText) return false;
		
		if (req.responseText.search(/<\s*frameset/i) != -1) return true;
		
		return false;
		
	},
	
	_saveWinLocal : function(win){
		if(this.isImage == null) 
				this.isImage =this.winMustBeImage(win);

		//if(this.isImage == true) this.imageUrl=printPages2Pdf.win2Image.saveCompleteWindow(this.workDir,win,this.canvas);
		
		return printPages2Pdf.saver.ContentSaver.captureWindowSync(this.workDir,win);
	},

	_saveUrlLocal:function(url){
		var win = printPages2Pdf.domLoader.getContentWindow(url,this.domLoadCallback, this._pageloadTimeout);

		if(this.isImage == null) 
				this.isImage =this.winMustBeImage(win);

		return printPages2Pdf.saver.ContentSaver.captureWindowSync(this.workDir,win);

	},
	
	_getEditWin : function(){
		var aktUrl=this.editUrl?this.editUrl:this.localUrl;
				
		//reset saved image, because its not longer valid
		this.imageUrl=null;
		var win= printPages2Pdf.domLoader.getContentWindow(aktUrl,this.domLoadCallback, this._pageloadTimeout);
			
		return win;		
		//return printPages2Pdf.saver.ContentSaver.captureWindowSync(this.workDir,win);
	},
	
	_saveEditWin:function(win){
				
		this.editUrl= printPages2Pdf.saver.ContentSaver.captureWindowSync(this.workDir,win);

	},
	
	
	_editCtrl:null,
	get editCtrl(){
		if(!this._editCtrl)
			this._editCtrl=new printPages2Pdf._editPage();
		
		return this._editCtrl;
	},
	
	getFinalUrl:function(){
		this.prepareCustomObjectsPref();		
		
		//if(!this.localUrl) this.preProcess();
		
		
		var aktUrl=this.localUrl;
		//if(this.editUrl) aktUrl=this.editUrl;
		
		if(this.isTextOnly == true|| this.isImage == true||this._outlineTitleOnly == "true"){
			var win = printPages2Pdf.domLoader.getContentWindow(aktUrl,this.domLoadCallback, this._pageloadTimeout);

			if(this.isTextOnly == true)
				printPages2Pdf.textOnly.cleanWindow(win);
			
			if(this._outlineTitleOnly == "true" && !this.isImage){
				this.editCtrl.cleanHeaders(win.document);
				this.editCtrl.setMainHeader(win.document,this.Title);
			}
	
			if(this.isImage == true)
				aktUrl=printPages2Pdf.win2Image.saveCompleteWindow(this.workDir,win,this.canvas,this.Title);
			else
				aktUrl=printPages2Pdf.saver.ContentSaver.captureWindowSync(this.workDir,win);			
			
		}

		return aktUrl;		

	},
}

var _tocObject=function(location,bTextOnly){
	_tocObject.parent.init.call(this,location,bTextOnly);
	this._Title=this.getTocTitle();
}

_tocObject.prototype=new _srcObject();
_tocObject.prototype.constructor = _tocObject;

_tocObject.parent=_srcObject.prototype;

_tocObject.prototype.getTocTitle = function(){
//	var title=RRprintPages2Pdf.prefs.getCharPref("wkhtml.topt.toc.captionText");	
	var title=RRprintPages2Pdf.prefs.getComplexValue("wkhtml.topt.toc.captionText",
      Components.interfaces.nsISupportsString).data;	
	if(!title)
		title=RRprintPages2Pdf.strb.GetStringFromName("toc.default.captionText")
	
	return title;
}

_tocObject.prototype.getFinalUrl=function(){
	//return this.getTocTitle();
	return "toc";
}

_tocObject.prototype._favIconUrl="chrome://printPages2Pdf/skin/contentTree.png";
_tocObject.prototype.sourceType="toc";

_tocObject.prototype.getConverterPrefs = function(){
	var gPrefObj=_tocObject.parent.getConverterPrefs.call(this);

   var prefs = Components.classes["@mozilla.org/preferences-service;1"]  
     .getService(Components.interfaces.nsIPrefService)  
     .getBranch("extensions.RRprintPages2Pdf.wkhtml.topt.");

   
	var obj={};
	var children = prefs.getChildList("",obj) ;

	for (var i = 0; i < children.length; i++) {
//		var prefVal=prefs.getCharPref(children[i]);
		var prefVal=prefs.getComplexValue(children[i],
      			Components.interfaces.nsISupportsString).data;
		if (prefVal) {
			gPrefObj[children[i]] = prefVal;
		}
	}

	//must be set for TOC
	gPrefObj["isTableOfContent"]="true"
	//gPrefObj["web.printMediaType"]="true"
	
	//gPrefObj["pagesCount"]="false";
	
	if(!("tocXsl" in gPrefObj) || !gPrefObj.tocXsl){
		var defTocXsl = RRprintPages2Pdf.ExtensionDir.clone();
		defTocXsl.append(RRprintPages2Pdf.g_const.EXTENSION_TEMPLATEDIR);
		defTocXsl.append(RRprintPages2Pdf.g_const.DEFTOCXSL_NAME);
				
		gPrefObj.tocXsl=defTocXsl.path;
	}
	
	//make the toc path to a file url = no problems with unicode
	var tmpFile = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
	tmpFile.initWithPath(gPrefObj.tocXsl);
	var IO=Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService); 
	gPrefObj.tocXsl = IO.newFileURI(tmpFile).spec;
	
	if(!gPrefObj["toc.captionText"])
		gPrefObj["toc.captionText"]=this.getTocTitle();
	
	//Set prefix, so that xsl transformation recognizes this as the configuration Title
	if(gPrefObj["toc.captionText"])
		gPrefObj["toc.captionText"] = RRprintPages2Pdf.g_const.TOC_DEFCAPTION_PREFIX +  gPrefObj["toc.captionText"]; 

		
	return gPrefObj;
}
 
