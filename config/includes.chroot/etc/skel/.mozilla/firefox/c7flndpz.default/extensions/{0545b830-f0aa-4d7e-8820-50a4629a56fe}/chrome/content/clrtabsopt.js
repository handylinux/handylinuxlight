var colorfulTabsOptions = {
saveDomains:function()
	{
	colorfulTabs.cl("saving domains");
	if(!document.getElementById("domainrows")) return;
	var domains =  document.getElementById("domainrows").getElementsByAttribute("rel","domain");
	var colors =  document.getElementById("domainrows").getElementsByAttribute("rel","color");
	dpref= new Array;
	var rawDomain,color;
	for(var i=0;i <domains.length ; i++)
		{
		rawDomain = domains[i].value
			rawDomain = rawDomain.replace(/^\s+|\s+$/, '');
		color = colors[i].value;
			color = color.replace(/^\s+|\s+$/, '');
		if(rawDomain == '') continue;
		if(color == '') continue;
		rawDomain.replace(/https:\/\//i,"")				 
		rawDomain=rawDomain.replace(/http:\/\//i,"")
		rawDomain.replace(/ftp:\/\//i,"")
		rawDomain.replace(/\//i,"");
		dpref.push(rawDomain+"~"+colors[i].value);
		}
	return dpref.join("`");
	},
	
savePalette:function(btnObjc)
	{
	btnClr = btnObjc.getAttribute('paletteclr');
	var params = {inn:{oldColor:btnClr, enabled:0}, out:null};
	window.openDialog('chrome://clrtabs/content/clrpkr.xul','_blank','modal,chrome,centerscreen,resizable=no, dialog=yes,close=no', params).focus();
	if (params.inn.enabled)
		{
		var clrNewColor = params.inn.oldColor;
		btnObjc.setAttribute('paletteclr',clrNewColor)
		btnObjc.setAttribute('style',"background-color:" + clrNewColor);
		}
	else
		{
		}
		var palette =  document.getElementById("palettecolors").getElementsByTagName("button");
		ppref= new Array;
		for(var i=0;i <palette.length ; i++)
			{
			ppref.push(palette[i].getAttribute('paletteclr'));
			}
		ppref = ppref.join("~")
		Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.clrtabs.").setCharPref("ppref",ppref);
	},
	
resetPrefs:function(){
		var ctPrefBranch = Components.classes["@mozilla.org/preferences-service;1"]
				.getService(Components.interfaces.nsIPrefBranch).getBranch("extensions.clrtabs.");
		var children = ctPrefBranch.getChildList("",{});
		var msg = 'ColorfulTabs Preferences have been reset.\nPlease restart browser.';
		for (var i=0;i<children.length;i++) {
			try {
				if(ctPrefBranch.prefHasUserValue(children[i])) {
					ctPrefBranch.clearUserPref(children[i]);
					}
			}
			catch(e) {
				colorfulTabs.cl(e);
				msg = 'Attempted ColorfulTabs Preferences reset.\nErrors have been logged to the console.\nPlease restart browser.';
			}
		}
		alert(msg);
	},
}