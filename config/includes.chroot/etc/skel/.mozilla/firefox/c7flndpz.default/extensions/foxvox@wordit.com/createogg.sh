#!/bin/bash

# get ogg file path and extension file paths, passed as command line args
extpath=$1
oggpath=$2

# remove marker indicating to extension that ogg file creation has completed
rm -v $extpath/oggdone.txt #>/dev/null
rm -v $extpath/noogg.txt #>/dev/null

# encode the existing wave as ogg file
oggenc $extpath/speak.wav -o $oggpath
error=$?
echo "oggenc err:$error"

	if [ "$error" -ne "0" ]; then
	{
		#create marker file
		echo "oggenc not found" > $extpath/noogg.txt
	} fi

	if [ "$error" -eq "0" ]; then
	{
	echo "" > $extpath/oggdone.txt
	} fi

